from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return '<h1>Hello World 2!</h1>'

@app.route('/tong')
def tong():
    return '<h1>Hello World Tong!</h1>'

@app.route('/nom')
def nom():
    return '<h1>Hello Nom</h1>'

if __name__ == '__main__':
    app.run(debug=True)
